#pragma once
//#pragma once是一个比较常用的C/C++杂注，
//只要在头文件的最开始加入这条杂注，
//就能够保证头文件只被编译一次。

#include<iostream>
#include<string>
using namespace std;

/*
本程序是使用Dijkstra算法实现求解最短路径的问题
采用的邻接矩阵来存储图
*/
//记录起点到每个顶点的最短路径的信息
struct Dis {
    string path;
    int value;
    bool visit;
    Dis() {
        visit = false;
        value = 0;
        path = "";
    }
};

struct AE {
    int a;
    int b;
    int weight;
    AE(int a = 0, int b = 0, int weight = 0) {
        this->a = a;
        this->b = b;
        this->weight = weight;
    }
};

class Graph_DG {
private:
    string* values;
    int vexnum;   //图的顶点个数
    int edge;     //图的边数
    int** arc;   //邻接矩阵
    Dis* dis;   //记录各个顶点最短路径的信息
    bool* visited;  //dfs过程中是否被访问过
    int* HLoop;  //记录哈密顿回路
    int count;   //辅助计数
    int begin;
    
public:
    //无参构造函数
    Graph_DG();
    //有参构造函数
    Graph_DG(int vexnum, int edge);
    //设置复制构造函数
    Graph_DG(Graph_DG& g);
    //析构函数
    ~Graph_DG();
    // 判断我们每次输入的的边的信息是否合法
    bool check_edge_value(int start, int end, int weight); //废弃无用？
    //创建图
    void createGraph(string* vertex, AE* ae);
    //打印邻接矩阵
    void print();
    //求最短路径
    void Dijkstra(int to);
    //打印最短路径
    string print_path(int);
    //是否所有的点都遍历完成：哈密顿图
    bool all_visited();
    //深度优先搜索，是否有经过s（头）和v的回路
    bool dfs(int s, int v);
    //返回游览全部景点的字符串
    string print_all_visited();
};
