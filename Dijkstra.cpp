#include"Dijkstra.h"

//默认构造函数
Graph_DG::Graph_DG() {
    
}
//构造函数
Graph_DG::Graph_DG(int vexnum, int edge) {
    //初始化顶点数和边数
    this->vexnum = vexnum;
    this->edge = edge;
    //为邻接矩阵开辟空间和赋初值
    arc = new int* [this->vexnum];
    dis = new Dis[this->vexnum];
    for (int i = 0; i < this->vexnum; i++) {
        arc[i] = new int[this->vexnum];
        for (int k = 0; k < this->vexnum; k++) {
            //邻接矩阵初始化为无穷大
            arc[i][k] = INT_MAX;
        }
    }
    //初始化顶点的数值
    values = new string[this->vexnum];
    //初始化哈密顿图的数据
    visited = new bool[this->vexnum];
    for (int i = 0; i < this->vexnum; i++)
    {
        visited[i] = false;
    }
    HLoop = new int[this->vexnum + 1];
    count = 0;
}

//复制构造函数
Graph_DG::Graph_DG(Graph_DG& g)
{
    
    this->vexnum = g.vexnum;
    this->edge = g.edge;
    this->count = count;
    //创建内存空间
    
    arc = new int* [this->vexnum];
    dis = new Dis[this->vexnum];
    //初始化顶点的数值
    values = new string[this->vexnum];
    //初始化哈密顿图的数据
    visited = new bool[this->vexnum];
    HLoop = new int[this->vexnum + 1];
    for (int i = 0; i < this->vexnum; i++) {
        arc[i] = new int[this->vexnum];
        dis[i] = g.dis[i];
        this->values[i] = g.values[i];
        visited[i] = g.visited[i];
        HLoop[i] = g.HLoop[i];
        for (int k = 0; k < this->vexnum; k++) {
            //邻接矩阵初始化为无穷大
            arc[i][k] = g.arc[i][k];
        }
    }
    HLoop[this->vexnum] = g.HLoop[g.vexnum];
    
}
//析构函数
Graph_DG::~Graph_DG() {
    
    delete[] dis;
    for (int i = 0; i < this->vexnum; i++) {
        delete[] this->arc[i];
    }
    delete[] arc;
    delete[] HLoop;
    delete[] visited;
    delete[] values;
}

// 判断我们每次输入的的边的信息是否合法
//顶点从1开始编号
bool Graph_DG::check_edge_value(int start, int end, int weight) {
    if (start<1 || end<1 || start>vexnum || end>vexnum || weight < 0) {
        return false;
    }
    return true;
}

void Graph_DG::createGraph(string* vertex, AE* ae) {
    /*
    cout << "请输入每条边的起点和终点（顶点编号从1开始）以及其权重" << endl;
    int start;
    int end;
    int weight;
    int count = 0;
    while (count != this->edge) {
        cin >> start >> end >> weight;
        //首先判断边的信息是否合法
        while (!this->check_edge_value(start, end, weight)) {
            cout << "输入的边的信息不合法，请重新输入" << endl;
            cin >> start >> end >> weight;
        }
        //对邻接矩阵对应上的点赋值
        arc[start - 1][end - 1] = weight;
        //无向图添加上这行代码
        arc[end - 1][start - 1] = weight;
        ++count;
    }
    */
    for (int i = 0; i < this->vexnum; i++)
    {
        values[i] = vertex[i];
    }
    for (int i = 0; i < this->edge; i++)
    {
        arc[ae[i].a][ae[i].b] = ae[i].weight;
        arc[ae[i].b][ae[i].a] = ae[i].weight;
    }
}

void Graph_DG::print() {
    cout << "图的邻接矩阵为：" << endl;
    int count_row = 0; //打印行的标签
    int count_col = 0; //打印列的标签
    //开始打印
    while (count_row != this->vexnum) {
        count_col = 0;
        while (count_col != this->vexnum) {
            if (arc[count_row][count_col] == INT_MAX)
                cout << "∞" << " ";
            else
                cout << arc[count_row][count_col] << " ";
            ++count_col;
        }
        cout << endl;
        ++count_row;
    }
}
void Graph_DG::Dijkstra(int begin) {
    this->begin = begin;
    //首先初始化我们的dis数组
    int i;
    for (i = 0; i < this->vexnum; i++) {
        //设置当前的路径
        dis[i].path = "景点：" + this->values[begin] + "-->景点：" + this->values[i];
        dis[i].value = arc[begin][i];
        dis[i].visit = false;
    }
    //设置起点的到起点的路径为0
    dis[begin].value = 0;
    dis[begin].visit = true;

    int count = 1;
    //计算剩余的顶点的最短路径（剩余this->vexnum-1个顶点）
    while (count != this->vexnum) {
        //temp用于保存当前dis数组中最小的那个下标
        //min记录的当前的最小值
        int temp = 0;
        int min = INT_MAX;
        for (i = 0; i < this->vexnum; i++) {
            if (!dis[i].visit && dis[i].value < min) {
                min = dis[i].value;
                temp = i;
            }
        }
        //cout << temp + 1 << "  "<<min << endl;
        //把temp对应的顶点加入到已经找到的最短路径的集合中
        dis[temp].visit = true;
        ++count;
        for (i = 0; i < this->vexnum; i++) {
            //注意这里的条件arc[temp][i]!=INT_MAX必须加，不然会出现溢出，从而造成程序异常
            if (!dis[i].visit && arc[temp][i] != INT_MAX && (dis[temp].value + arc[temp][i]) < dis[i].value) {
                //如果新得到的边可以影响其他为访问的顶点，那就就更新它的最短路径和长度
                dis[i].value = dis[temp].value + arc[temp][i];
                dis[i].path = dis[temp].path + "-->景点：" + this->values[i];
            }
        }
    }

}
string Graph_DG::print_path(int to) {
    string str;
    str = "v" + to_string(begin);
    
    str = "以" + str + "为起点的图的最短路径为：\n";
    for (int i = 0; i != this->vexnum; i++) {
        
        if (dis[i].value != INT_MAX && i == to) {
            //cout << dis[i].path << "=" << dis[i].value << endl;
            str = str + dis[i].path + "=" + to_string(dis[i].value) + "\n";
        }
        else if(i == to ){
            // cout << dis[i].path << "是无最短路径的" << endl;
            str = str + dis[i].path + "是无最短路径的\n";
        }
        /*
        if (i == to) {
            str = str + dis[i].path + "=" + to_string(dis[i].value) + "\n";
            break;
        }
        */
    }
    return str;
    
}

bool Graph_DG::all_visited() {
    for (int i = 0; i < this->vexnum; i++)
    {
        if (!visited[i]) return false; //没有访问完返回false
    }
    return true;
}

bool Graph_DG::dfs(int s, int v)
{
    visited[v] = true;
    HLoop[count++] = v;
    for (int i = 0; i < this->vexnum; i++)
    {
        //若v与i之间有边
        if (arc[v][i] != INT_MAX) {
            if (!visited[i]) //没有访问过i
            {
                if (dfs(s, i)) {
                    //cout << v << ":" << i << "=" << arc[v][i] << endl;
                    //HLoop[count--] = s;
                    return true;
                }
            }
            else if (i == s && all_visited()) {
                //cout << v << ":" << i << "=" << arc[v][i] << endl;
                HLoop[count++] = v;
                HLoop[this->vexnum] = i;
                //HLoop[count--] = s;
                return true;
            }
        }
    }
    visited[v] = false;
    count--;
    return false;
}

string Graph_DG::print_all_visited()
{
    
    string str;
    if (!all_visited()) {
        str = "错误！系统中没有哈密顿回路！";
        return str;
    }
    str = str + "从出口开始游览全部景点的路径是：\n";
    for (int i = 0; i <= this->vexnum; i++)
    {

        str = str + "第" + to_string(i) + "个景点" + this->values[HLoop[i]] + "\n";
    }
    str = str + "从出口开始游览全部景点的路径是：\n";
    return str;
}