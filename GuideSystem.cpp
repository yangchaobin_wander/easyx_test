#include "GuideSystem.h"

//析构函数
GuideSystem::~GuideSystem()
{
	//清除图相关信息的内存
	delete[] vertex;
	delete[] af;
}
//将char 转 TCHAR
TCHAR* GuideSystem::CharToTCHAR(const char* _char, TCHAR* tchar)
{
	int iLength;

	iLength = MultiByteToWideChar(CP_ACP, 0, _char, strlen(_char) + 1, NULL, 0);
	MultiByteToWideChar(CP_ACP, 0, _char, strlen(_char) + 1, tchar, iLength);
	return tchar;
}

//封装函数，将string转换为Tchar,最多转换2000个字
TCHAR* GuideSystem::StirngToTCHAR(const string str, TCHAR* tchar)
{
	char tmp[2000];
	strcpy_s(tmp, str.c_str());
	CharToTCHAR(tmp, tchar);
	return tchar;
}

//将wchar_t转换为int
int GuideSystem::wcharToInt(wchar_t* tchar)
{
	int iLength;
	char _char[50];
	iLength = WideCharToMultiByte(CP_ACP, 0, tchar, -1, NULL, 0, NULL, NULL);
	WideCharToMultiByte(CP_ACP, 0, tchar, -1, _char, iLength, NULL, NULL);
	int id = atoi(_char);
	return id;
}

//按钮组件
void GuideSystem::button(int x, int y, int w, int h, TCHAR* text)
{
	setbkmode(TRANSPARENT);
	COLORREF color = RGB(120, 250, 110);
	setfillcolor(color);
	fillroundrect(x, y, x + w, y + h, 10, 10);
	//输出字符串
	TCHAR s1[] = L"黑体";
	settextstyle(30, 0, s1);
	//TCHAR s[50] = L"hello";
	
	int tx = x + (w - textwidth(text)) / 2;
	int ty = y + (h - textheight(text)) / 2;
	outtextxy(tx, ty, text);

}

//**********构造函数****************
GuideSystem::GuideSystem() {
	//导入相关数据
	//读取文件数据
	//使用vertex和ae分别保存顶点数据和权重数据
	ifstream v_file;
	v_file.open("vertex.txt", ios::in);
	int vertexNum;
	v_file >> vertexNum;
	cout << vertexNum << endl;
	string* vertex = new string[vertexNum];
	int count = 0;
	while (count < vertexNum)
	{
		v_file >> vertex[count++];
	}

	ifstream e_file("edge.txt", ios::in);
	int edgeNum;
	e_file >> edgeNum;
	AE* af = new AE[edgeNum];
	count = 0;
	while (count < edgeNum)
	{
		e_file >> af[count].a >> af[count].b >> af[count].weight;
		count++;
	}

	//建立一个图对象
	Graph_DG init_graph(vertexNum, edgeNum);
	init_graph.createGraph(vertex, af);
	graph = init_graph;
	//回溯法构造哈密顿图
	graph.dfs(0, 0);
	

	//获取从0开始的最短路径
	graph.Dijkstra(0);

	
	
	// ***************分界线************
	//设置UI界面
	win_width = 850, win_height = 600;
	initgraph(win_width, win_height); //初始化窗口
	setbkcolor(WHITE);  //设置背景颜色，默认黑色
	cleardevice();     //清屏
	//播放歌曲
	mciSendString(L"open 爱人错过.mp3 repeat", 0, 0, 0);  //后面三个值与音乐有关，使用默认3个0
	mciSendString(L"play 爱人错过.mp3 repeat", 0, 0, 0);
	start();
	
}

//开始界面
void GuideSystem::start()
{
	cleardevice();     //清屏
	//绘制开始界面
	loadimage(&img, L"imgs/教学楼.jpg", win_width, win_height, true);

	putimage(0, 0, &img); //left, top, *buf


	//尝试设置按钮
	TCHAR s[50] = L"开始";
	//按钮的宽、高
	int width = 170;
	int height = 50;
	int btn_x = (win_width - width) / 2;
	int btn_y = (win_height - height) / 2;
	button(btn_x, btn_y, width, height, s);


	while (1) { //重复执行
		
		if (peekmessage(&msg, EM_MOUSE))
		{
			if (msg.message == WM_LBUTTONUP) //代表触发了鼠标的点击松开事件
			{
				if (msg.x > btn_x && msg.x < (btn_x + width) && msg.y > btn_y && msg.y < (btn_y + height)) {
					Sleep(400); //线程休眠400毫秒
					break;
				}
			}
		}
	}
	menu();  //调用menu函数
}

//主菜单界面
void GuideSystem::menu()
{
	draw_menu();

	//设置点击事件
	while (true)
	{
		if (peekmessage(&msg, EM_MOUSE))
		{
			if (msg.message == WM_LBUTTONUP) //代表触发了鼠标的点击松开事件
			{
				//返回按钮
				if (msg.x > 20 && msg.x < (20 + 125) && msg.y > 20 && msg.y < (20 + 50)) {
					Sleep(400); //线程休眠400毫秒
					button(btn_x, 200, width, height, (TCHAR*)L"实验按钮");
					break;
				}
				//查看地图按钮
				else if (msg.x > btn_x && msg.x < (btn_x + width) && msg.y > 100 && msg.y < (100 + height))
				{
					Sleep(400);
					check_map(); //查看地图
					draw_menu();
				}
				//游园路线按钮
				else if (msg.x > btn_x && msg.x < (btn_x + width) && msg.y > 160 && msg.y < (160 + height))
				{
					Sleep(400);
					visit_whole_school(); //查看游园路线
					draw_menu();
				}
				
				//查询路线按钮
				else if (msg.x > btn_x && msg.x < (btn_x + width) && msg.y > 220 && msg.y < (220 + height))
				{
					Sleep(400);
					search_route();  //查询游园路线界面
					draw_menu();
				}
			}

		}
	}
	start();
}

//重新渲染主菜单界面
void GuideSystem::draw_menu()
{
	//清屏
	cleardevice();
	//设置背景图片
	loadimage(&img, L"imgs/主界面.jpg", win_width, win_height, true);
	putimage(0, 0, &img);

	//设置左上角的返回按钮
	btn_return();
	//设置主菜单的按钮：查看地图，游园路线，查询路线，更多功能（开发中）
	//按钮设置水平居中的btn_x
	width = 170;
	height = 50;
	btn_x = (win_width - width) / 2;
	button(btn_x, 100, width, height, (TCHAR*)L"查看地图");
	button(btn_x, 160, width, height, (TCHAR*)L"游园路线");
	button(btn_x, 220, width, height, (TCHAR*)L"查询路线");
	button((win_width - 280) / 2, 280, 280, height, (TCHAR*)L"更多功能（开发中）");
}

//设置查看地图界面
void GuideSystem::check_map()
{
	cleardevice(); //清屏
	//设置背景图片
	loadimage(&img, L"imgs/校区地图.jpg", win_width, win_height, true);
	putimage(0, 0, &img);

	//设置返回按钮
	btn_return();
	while (true)
	{
		if (peekmessage(&msg, EM_MOUSE))
		{
			if (msg.message == WM_LBUTTONUP) //代表触发了鼠标的点击松开事件
			{
				//返回按钮
				if (msg.x > 20 && msg.x < (20 + 125) && msg.y > 20 && msg.y < (20 + 50)) {
					Sleep(400); //线程休眠400毫秒
					break;
				}
			}

		}
	}

}
//设置查询路线界面
void GuideSystem::search_route()
{
	int from;
	int to;
	//输入出发地编号
	wchar_t num[50];
	InputBox(num, 10, L"出发地编号：", L"查询路线");
	from = wcharToInt(num);
	//输入目的地编号
	InputBox(num, 10, L"目的地编号：", L"查询路线");
	to = wcharToInt(num);
	//调用graph的函数进行查询
	graph.Dijkstra(from);
	
	//返回查询结果
	string route = graph.print_path(to);
	TCHAR t_path[1000];
	StirngToTCHAR(route, t_path);
	MessageBox(0, t_path, L"查询路线", 1);
}

//设置游园路线
void GuideSystem::visit_whole_school()
{

	//输出文字
	string path = graph.print_all_visited();
	TCHAR t_path[1000];
	StirngToTCHAR(path, t_path);
	MessageBox(0, t_path, L"游园路径", 1);

}
//返回按钮
void GuideSystem::btn_return()
{
	//绘制背景
	COLORREF color = RGB(126, 163, 216);
	setfillcolor(color);
	fillroundrect(20, 20, 20 + 125, 20 + 50, 10, 10);
	//字体格式
	TCHAR s1[] = L"楷体";
	settextstyle(30, 0, s1);
	
	//文字
	TCHAR text[] = L"返回";
	int tx = 20 + (125 - textwidth(text)) / 2;
	int ty = 20 + (50 - textheight(text)) / 2;
	outtextxy(tx, ty, text);
}




/*
	本程序easyx不支持string，所以输出中文很困难
	解决方案：
		将string先转化为char数组
		之后再将char数组转化为TCHAR，这个过程要注意转码的问题

	2.atoi函数可将char *转化为int
*/