#pragma once
#include<string>
#include<iostream>
#include<vector>
#include<fstream>
#include<cstdlib>
#include<windows.h>
#include<cstdio>
#include<graphics.h>
#include<time.h>
//#include<atlstr.h>
#include<mmsystem.h>                   //添加多媒体头文件
#pragma comment(lib,"winmm.lib")       //添加多媒体库
#include "Dijkstra.h"
using namespace std;

class GuideSystem {
public:
	GuideSystem();
	//设置按钮
	void button(int, int, int, int, TCHAR*);
	~GuideSystem();
	
private:
	short win_width, win_height;   //定义窗口的宽度和高度
	IMAGE img; //背景图片
	ExMessage msg;  //获取事件信息
	int btn_x, width, height; //普通按钮的宽，高和x坐标

	//设置图的相关信息
	Graph_DG graph;
	//设置变量接受图的相关信息
	string* vertex;
	AE* af;
	//将char 转 tchar
	TCHAR* CharToTCHAR(const char* _char, TCHAR* tchar);
	//将string转换为Tchar,最多转换2000个字
	TCHAR* StirngToTCHAR(const string str, TCHAR* tchar);
	//将wchar转化为int
	int wcharToInt(wchar_t* tchar);
	//设置开始界面
	void start();
	//设置主菜单界面
	void menu();
	//重新渲染主菜单界面
	void draw_menu();
	//返回按钮
	void btn_return();
	//设置查看地图界面
	void check_map();
	//设置查询路线界面
	void search_route();
	//查看游园路线
	void visit_whole_school();
};